package es.itplus.mongorepositorybase;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
import com.mongodb.DB;
import com.mongodb.MongoClient;
import java.net.UnknownHostException;
import javax.servlet.ServletContext;
import javax.servlet.ServletContextEvent;
import javax.servlet.ServletContextListener;
import org.jongo.Jongo;

/**
 *
 * @author javier
 */
public class BBDD implements ServletContextListener {

    private static Jongo jongo;
    private static String DATABASE_HOST;// System.getenv("OPENSHIFT_MONGODB_DB_HOST");
    private static Integer DATABASE_PORT; //Integer.valueOf(System.getenv("OPENSHIFT_MONGODB_DB_PORT"));
    private static String usuarioPrueba;
    private static String password;
    private static String nombreBaseDatos;

    private static Jongo openConnection() throws UnknownHostException {

        MongoClient mongoClient = new MongoClient(DATABASE_HOST, DATABASE_PORT);
        DB db = mongoClient.getDB(nombreBaseDatos);
        jongo = new Jongo(db);
        boolean auth = db.authenticate(usuarioPrueba, password.toCharArray());
        return jongo;
    }

    @Override
    public void contextInitialized(ServletContextEvent sce) {
        try {
            ServletContext ctx = sce.getServletContext();

            String valor = ctx.getInitParameter("es.itplus.app.DATABASE_HOST");
            DATABASE_HOST = (valor != null) ? valor : "localhost";// System.getenv("OPENSHIFT_MONGODB_DB_HOST");

            valor = ctx.getInitParameter("es.itplus.app.DATABASE_PORT");
            DATABASE_PORT = (valor != null) ? Integer.getInteger(valor) : 27017; //Integer.valueOf(System.getenv("OPENSHIFT_MONGODB_DB_PORT"));

            valor = ctx.getInitParameter("es.itplus.app.usuarioPrueba");
            usuarioPrueba =  (valor != null) ? valor : "usuarioPrueba1";

            valor = ctx.getInitParameter("es.itplus.app.password");
            password = (valor != null) ? valor : "pass";

            valor = ctx.getInitParameter("es.itplus.app.nombreBaseDatos");
            nombreBaseDatos = (valor != null) ? valor : "basePrueba1";

            jongo = openConnection();
        } catch (UnknownHostException ex) {
        }
    }

    @Override
    public void contextDestroyed(ServletContextEvent sce) {

    }

    public static Jongo getDB() throws UnknownHostException {
        if (jongo == null) {
            jongo = openConnection();
        }
        return jongo;
    }

}
