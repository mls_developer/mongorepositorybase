/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package es.itplus.mongorepositorybase.entidad;

import javax.xml.bind.annotation.XmlRootElement;
import org.jongo.marshall.jackson.oid.MongoObjectId;

/**
 *
 * @author miguel
 */
@XmlRootElement
public class Pais {

    @MongoObjectId
    private String _id;
    private String nombre;

    public String getId() {
        return _id;
    }

    public void setId(String id) {
        this._id = id;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }
    
    
}
